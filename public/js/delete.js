function deletarMesa(mesaId){
    var url = '/api/mesa/' + mesaId;

$.ajax({
    url: url,
    type: 'DELETE',
    headers: {"X-AUTH": $('#token').val()},
    success: function (data, textStatus, jqXHR) {
        alert("Mesa excluída com sucesso.");
        location.reload();
    },
    error: function (jqXHR, textStatus, errorThrown) {
        alert(textStatus);
    }
});
}

function deletarPrato(pratoId) {
    var url = '/api/prato/' + pratoId;

    $.ajax({
        url: url,
        type: 'DELETE',
        headers: {"X-AUTH": $('#token').val()},
        success: function (data, textStatus, jqXHR) {
            alert("Prato excluída com sucesso.");
            location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
}