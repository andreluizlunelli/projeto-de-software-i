$(".enviarSugestao").click(function (e) {
    e.preventDefault();

    var SendInfo = {"mesa": $('#inputMesa').val(), "descricao": $('#inputSugestao').val()};

    $.ajax({
        url: "/api/sugestao",
        type: "POST",
        data: JSON.stringify(SendInfo),
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            alert("Sugestão cadastrada com sucesso.");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
});

$(".enviarPrato").click(function (e) {
    e.preventDefault();

    var type;
    var url = "/api/prato";

    if (sessionStorage.getItem("id") == ""){
        type = "POST";
    } else {
        type = "PUT";
        url += "/"+sessionStorage.getItem("id");
    };

    var SendInfo = {"descricao": $('#inputDescricao').val(), "preco": $('#inputPreco').val()};

    $.ajax({
        url: url,
        type: type,
        data: JSON.stringify(SendInfo),
        headers: {"X-AUTH": $('#token').val()},
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {

            alert("Prato cadastrada com sucesso.");
            location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alert(textStatus);
        }
    });
});

$(".enviarMesa").click(function (e) {
    e.preventDefault();

    var type = "POST";
    var url = "/api/mesa";

    if (sessionStorage.getItem("id") == ""){
        type = "POST";
    } else {
        type = "PUT";
        url += "/"+sessionStorage.getItem("id");
    };

    var SendInfo = {"numero": $('#inputNumero').val(), "assentos": $('#inputAssentos').val()};

    $.ajax({
        url: url,
        type: type,
        data: JSON.stringify(SendInfo),
        headers: {"X-AUTH": $('#token').val()},
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            // console.log(data)
            // $('#mensagem-alert').text("Mesa cadastrada com sucesso.");
            // $('#mensagem-alert').show();
            showMessage("Mesa cadastrada com sucesso.");

            // $(".alert.alert-secondary.a2").text("Mesa cadastrada com sucesso.");
            // $(".alert.alert-secondary.a2").show();
            setTimeout(function () {}, 2000);
            location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // alert(data);
            alert(textStatus);
        }
    });
});
