# Rodízio

Aplicação em php para adicionar sugestões de cardápio ao restarurante.

## Qual o problema a ser resolvido: 	
Os Clientes de um rodízio geralmente esperam muito tempo até chegar o sabor desejado. Isso acaba acarretando uma insatisfação inferior por conta da espera e tempo de permanência desnecessária no estabelecimento, que poderia ceder lugar a os demais clientes.

## Requisitos para rodar o projeto
* Php
* Postgres(9.6)
* Composer(arquivo já está no projeto ``composer.phar``)
* Um cliente para enviar requisições(ex: postman)

## Passos para instalar o projeto

* Clonar ou fazer download desse projeto na branch master
* php composer.phar install
* criar o banco ``CREATE DATABASE rodizio;``
* criar tabelas do banco ``"vendor/bin/doctrine" orm:schema-tool:create``
* ``cd public && php -S localhost:8080``
