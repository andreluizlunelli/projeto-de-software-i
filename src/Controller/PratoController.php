<?php
/**
 * Created by PhpStorm.
 * User: André Luiz
 * Date: 22/08/2017
 * Time: 18:53
 */

namespace Src\Controller;

use Cocur\Slugify\Slugify;
use Slim\Exception\NotFoundException;
use Src\Model\Entity\Prato;
use Src\Model\Entity\Sugestao;
use Src\System\Database;

class PratoController
{
    public function get(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        $all = Database::getEm()->getRepository(Prato::class)->findAll();

        $array = [];
        /** @var Sugestao $item */
        foreach ($all as $item)
            $array[] = $item->toArray();

        return $response->withJson($array);
    }

    public function getOne(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        $entity = Database::getEm()->getRepository(Prato::class)
            ->find((int) $args['id']);

        if (empty($entity))
            return $response->withJson('nao foi encontrado')->withStatus(404);

        return $response->withJson(($entity)->toArray());
    }

    public function post(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        try {
            if (strlen((string) $request->getBody()) < 1)
                throw new \InvalidArgumentException('Corpo da requisicao vazio. Necessario enviar: descricao, preco');

            $body = json_decode((string)$request->getBody(), true);

            $descricao = $body['descricao'] ?? null;
            $preco = $body['preco'] ?? null;

            if (empty($descricao) || empty($preco)) {
                throw new \InvalidArgumentException('descricao ou preco esta faltando na requisicao');
            }

            $slugify = new Slugify();
            $slug = $slugify->slugify($descricao);

            $prato = new Prato($descricao, $slug, (float) $preco);

            Database::getEm()->persist($prato);
            Database::getEm()->flush();

            return $response
                ->withHeader('location', $_SERVER['PATH_INFO'] . '/' . $prato->getId())
                ->withStatus(201)
                ->withJson('ok');

        } catch (\Throwable $e) {
            return $response->withJson(['erro' => $e->getMessage()], 500);
        }
    }

    public function put(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        try {
            if (strlen((string) $request->getBody()) < 1)
                throw new \InvalidArgumentException('Corpo da requisicao vazio');

            $body = json_decode((string)$request->getBody(), true);

            $descricao = $body['descricao'] ?? null;
            $preco = $body['preco'] ?? null;

            if (empty($descricao) && empty($preco)) {
                throw new \InvalidArgumentException('descricao ou preco esta faltando na requisicao');
            }

            /** @var Prato $prato */
            $prato = Database::getEm()->getRepository(Prato::class)->find((int)$args['id']);

            if (empty($prato))
                throw new \Exception('Prato não existe');

            if (!empty($descricao)) {
                $slugify = new Slugify();
                $slug = $slugify->slugify($descricao);
                $prato->setSlug($slug);
                $prato->setDescricao($descricao);
            }
            if (!empty($preco))
                $prato->setPreco($preco);

            Database::getEm()->persist($prato);
            Database::getEm()->flush();

            return $response->withJson($prato->toArray());

        } catch (\Throwable $e) {
            return $response->withJson(['erro' => $e->getMessage()], 500);
        }
    }

    public function delete(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        try {
            /** @var Prato $prato */
            $prato = Database::getEm()->getRepository(Prato::class)->find((int)$args['id']);

            if (empty($prato)) {
                throw new NotFoundException($request, $response);
            }

            Database::getEm()->remove($prato);
            Database::getEm()->flush();

            return $response->withJson('ok');

        } catch (NotFoundException $e) {
            return $response->withJson(['erro' => 'prato nao encontrado'])->withStatus(404);
        } catch (\Throwable $e) {
            return $response->withJson(['erro' => $e->getMessage()], 500);
        }
    }

}