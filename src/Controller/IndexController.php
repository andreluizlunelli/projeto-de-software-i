<?php
/**
 * Created by PhpStorm.
 * User: Sony
 * Date: 05/09/2017
 * Time: 19:28
 */

namespace Src\Controller;

use Src\Model\Entity\Exemplo;
use Src\System\Database;
use Src\System\App;

/**
 * @deprecated Foi utilizado apenas para demonstração
 *
 * Class IndexController
 * @package Src\Controller
 */
class IndexController
{
    public function index(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        if ($request->isPost()) {
            $texto = $_POST["texto"];
            if (strlen($texto) > 1) {
                $exemplo = new Exemplo($texto);

                Database::getEm()->persist($exemplo);
                Database::getEm()->flush();

                $args['exemplo'] = $exemplo;
            } else {
                $texto_semdoctrine = $_POST['texto_semdoctrine'];

                $con = \pg_connect("host=localhost port=5432 dbname=rodizio user=postgres password=postgres");


                // 1 ) ; drop table exemplo; --

                pg_query($con, "insert into exemplo (descricao) values ($texto_semdoctrine)");

                \pg_close($con);

            }
        }

        $args['lista'] = Database::getEm()->getRepository(Exemplo::class)->findAll();

        return (App::getApp()->getContainer()->get('renderer'))->render($response, 'index.phtml', $args);
    }
}