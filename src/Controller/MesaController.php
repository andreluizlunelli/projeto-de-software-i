<?php
/**
 * Created by PhpStorm.
 * User: André Luiz
 * Date: 22/08/2017
 * Time: 18:53
 */

namespace Src\Controller;

use Slim\Exception\NotFoundException;
use Src\Model\Entity\Mesa;
use Src\Model\Entity\Prato;
use Src\Model\Entity\Sugestao;
use Src\System\Database;

class MesaController
{
    public function get(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        $all = Database::getEm()->getRepository(Mesa::class)->findAll();

        $array = [];
        /** @var Sugestao $item */
        foreach ($all as $item)
            $array[] = $item->toArray();

        return $response->withJson($array);
    }

    public function getOne(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        $entity = Database::getEm()->getRepository(Mesa::class)
            ->find((int) $args['id']);

        if (empty($entity))
            return $response->withJson('nao foi encontrado')->withStatus(404);

        return $response->withJson(($entity)->toArray());
    }

    public function post(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        try {
            if (strlen((string) $request->getBody()) < 1)
                throw new \InvalidArgumentException('Corpo da requisicao vazio. Necessario enviar: descricao, preco');

            $body = json_decode((string)$request->getBody(), true);

            $numMesa = (int)$body['numero'] ?? null;
            $numAssentos = (int)$body['assentos'] ?? null;

            if (empty($numMesa) || empty($numAssentos)) {
                throw new \InvalidArgumentException('numero ou assentos esta faltando na requisicao');
            }

            $mesa = new Mesa($numMesa, $numAssentos);

            Database::getEm()->persist($mesa);
            Database::getEm()->flush();

            return $response
                ->withHeader('location', $_SERVER['PATH_INFO'] . '/' . $mesa->getId())
                ->withStatus(201)
                ->withJson('ok');

        } catch (\Throwable $e) {
            return $response->withJson(['erro' => $e->getMessage()], 500);
        }
    }

    public function put(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        try {
            if (strlen((string) $request->getBody()) < 1)
                throw new \InvalidArgumentException('Corpo da requisicao vazio');

            $body = json_decode((string)$request->getBody(), true);

            $numAssentos = (int)$body['assentos'] ?? null;

            if (empty($numAssentos)) {
                throw new \InvalidArgumentException('assentos esta faltando na requisicao');
            }

            /** @var Mesa $mesa */
            $mesa = Database::getEm()->getRepository(Mesa::class)->find((int)$args['id']);
            $mesa->setAssentos($numAssentos);

            Database::getEm()->persist($mesa);
            Database::getEm()->flush();

            return $response->withJson($mesa->toArray());

        } catch (\Throwable $e) {
            return $response->withJson(['erro' => $e->getMessage()], 500);
        }
    }

    public function delete(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        try {
            /** @var Mesa $mesa */
            $mesa = Database::getEm()->getRepository(Mesa::class)->find((int)$args['id']);

            if (empty($mesa)) {
                throw new NotFoundException($request, $response);
            }

            Database::getEm()->remove($mesa);
            Database::getEm()->flush();

            return $response->withJson('ok');

        } catch (NotFoundException $e) {
            return $response->withJson(['erro' => 'mesa não encontrada'])->withStatus(404);
        } catch (\Throwable $e) {
            return $response->withJson(['erro' => $e->getMessage()], 500);
        }
    }

}