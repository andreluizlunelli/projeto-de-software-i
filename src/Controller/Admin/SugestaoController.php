<?php
/**
 * User: André Lunelli <andre@microton.com.br>
 * Date: 01/11/2017
 */

namespace Src\Controller\Admin;

use Psr\Container\ContainerInterface;
use Src\Model\Entity\Sugestao;
use Src\System\Database;

class SugestaoController
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->entityManager = Database::getEm();
    }

    public function sugestoes(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        /**
         * Adicionar uma flag na sugestão de `pedidoAtendido` algo assim pra poder filtrar e buscar somente as sugestões não atendidas
         */
        $args['sugestoesEnviadas'] = $this->entityManager->getRepository(Sugestao::class)->findAll();
        return $this->container->get('view')->render($response, 'admin/sugestao.twig', $args);
    }
}