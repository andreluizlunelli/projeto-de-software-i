<?php
/**
 * User: André Lunelli <andre@microton.com.br>
 * Date: 03/11/2017
 */

namespace Src\Controller\Site;

use Psr\Container\ContainerInterface;
use Src\Controller\TokenController;

class LoginController
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function login(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        if ($request->isPost()) {
            $body = $request->getParsedBody();

            $login = $body['login'] ?? '';
            $pass = $body['pass'] ?? '';

            if ( empty($login) || empty($pass)) {
                $args['mensagem'] = 'Necessario informar as credenciais';
                return $this->container->get('view')->render($response, 'admin/login.twig', $args);
            }

            if ($login != "admin" || $pass != "admin") {
                $args['mensagem'] = 'Usuario não autenticado.';
                return $this->container->get('view')->render($response, 'admin/login.twig', $args);
            }

            $tokenController = new TokenController($this->container);
            $_SESSION['admin']['token'] = ($tokenController->createToken())->__toString();
            return $response->withRedirect('/admin', 200);
        }

        return $this->container->get('view')->render($response, 'admin/login.twig', $args);
    }

    public function logoff(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        session_destroy();
        return $response->withRedirect('/', 200);
    }

}