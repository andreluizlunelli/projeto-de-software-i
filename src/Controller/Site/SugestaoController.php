<?php
/**
 * User: André Lunelli <andre@microton.com.br>
 * Date: 01/11/2017
 */

namespace Src\Controller\Site;

use Psr\Container\ContainerInterface;
use Src\Model\Entity\Mesa;
use Src\System\Database;

class SugestaoController
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->entityManager = Database::getEm();
    }

    public function apresentarForm(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        $args['mesas'] = $this->entityManager->getRepository(Mesa::class)->findAll();
        return $this->container->get('view')->render($response, 'sugestaoo.twig', $args);
    }
}