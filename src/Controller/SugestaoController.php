<?php
/**
 * Created by PhpStorm.
 * User: André Luiz
 * Date: 22/08/2017
 * Time: 18:53
 */

namespace Src\Controller;

use Src\Model\Entity\Mesa;
use Src\Model\Entity\Sugestao;
use Src\System\Database;

class SugestaoController
{
    public function get(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        $all = Database::getEm()->getRepository(Sugestao::class)->findAll();

        $array = [];
        /** @var Sugestao $item */
        foreach ($all as $item)
            $array[] = $item->toArray();

        return $response->withJson($array);
    }

    public function getOne(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        $entity = Database::getEm()->getRepository(Sugestao::class)
            ->find((int) $args['id']);

        if (empty($entity))
            return $response->withJson('nao foi encontrado')->withStatus(404);

        return $response->withJson(($entity)->toArray());
    }

    public function post(\Slim\Http\Request $request, \Slim\Http\Response $response, $args)
    {
        try {
            $body = json_decode((string) $request->getBody(), true);

            $descricao = (string) $body['descricao'] ?? null;
            $numMesa = (int) $body['mesa'] ?? null;

            if (empty($descricao) || empty($numMesa)) {
                throw new \InvalidArgumentException('faltando parametros mesa e descricao');
            }

            $em = Database::getEntityManager();

            $mesa = $em->getRepository(Mesa::class)->findOneBy(['numero' => $numMesa]);
            if (empty($mesa)) {
                throw new \InvalidArgumentException('mesa nao existe');
            }

            $sugestao = new Sugestao($descricao, $mesa);

            $em->persist($sugestao);
            $em->flush();

            return $response
                ->withHeader('location', $_SERVER['PATH_INFO'] . '/' . $sugestao->getId())
                ->withStatus(201)
                ->withJson('ok');

        } catch (\Exception $e) {
            return $response->withJson(['erro' => $e->getMessage()], 500);
        }
    }
}