<?php
/**
 * User: André Lunelli <andre@microton.com.br>
 * Date: 03/11/2017
 */

namespace Src\Middleware;

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class AutorizacaoMiddleware
{
    /**
     * Example middleware invokable class
     *
     * @param  ServerRequestInterface $request  PSR7 request
     * @param  ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next): ResponseInterface
    {
        $stringToken = $_SESSION['admin']['token'] ?? '';

        if (strlen($stringToken) < 1)
            return $response->withRedirect('/login', 200);

        $token = (new Parser())->parse( $_SESSION['admin']['token'] );

        $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
        $data->setIssuer('http://localhost:8080');
        $data->setAudience('http://localhost:8080');

        if ( ! $token->validate($data)) {
            return $response->withRedirect('/login', 200);
        }

        $response = $next($request, $response);

        return $response;
    }
}