<?php
/**
 * User: André Lunelli <andre@microton.com.br>
 * Date: 11/09/2017
 */

namespace Src\Middleware;

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

class AuthMiddleware
{
    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke(\Slim\Http\Request $request, \Slim\Http\Response $response, $next)
    {
        /**
         * TODO Por a chamada da validação do api aqui dentro desse metodo
         *
         * @see https://pt.stackoverflow.com/questions/28235/colocar-authorization-basic-na-api
         */

        if ( ! isset( $_SERVER['HTTP_X_AUTH']) || empty( $_SERVER['HTTP_X_AUTH'])) {
            return $response->withJson('Necessário informar o token de acesso. Enviar post em /token')->withStatus(401);
        }

        $token = (new Parser())->parse( $_SERVER['HTTP_X_AUTH'] );

        $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
        $data->setIssuer('http://localhost:8080');
        $data->setAudience('http://localhost:8080');

        if ( ! $token->validate($data))
            return $response
                ->withJson('Token expirado')
                ->withStatus(500);

        $response = $next($request, $response);
        return $response;

    }
}