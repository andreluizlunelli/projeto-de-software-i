<?php
/**
 * User: André Lunelli <andre@microton.com.br>
 * Date: 06/09/2017
 */

namespace Src\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="exemplo")
 *
 * Class Exemplo
 * @package Src\Model\Entity
 */
class Exemplo
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     *
     * @var integer
     */
    private $descricao;

    public function __construct(string $descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return int
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param int $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

}