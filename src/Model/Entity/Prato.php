<?php
/**
 * User: André Lunelli <andre@microton.com.br>
 * Date: 23/08/2017
 */

namespace Src\Model\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Src\Model\Repository\PratoRepository")
 * @ORM\Table(name="prato")
 *
 * Class Prato
 * @package Src\Model\Entity
 */
class Prato implements ToExpose
{
    use TimestampTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $descricao;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    private $preco;

    /**
     * Prato constructor.
     * @param string $descricao
     * @param string $slug
     * @param float $preco
     */
    public function __construct($descricao, $slug, $preco)
    {
        $this->setCreatedAt(new \DateTime('now'));
        $this->descricao = $descricao;
        $this->slug = $slug;
        $this->preco = $preco;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDescricao(): string
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao(string $descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return float
     */
    public function getPreco(): float
    {
        return $this->preco;
    }

    /**
     * @param float $preco
     */
    public function setPreco(float $preco)
    {
        $this->preco = $preco;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId()
            , 'descricao' => $this->getDescricao()
            , 'slug' => $this->getSlug()
            , 'preco' => $this->getPreco()
            , 'createdAt' => $this->getCreatedAt()->format('Y-m-d H:i:s')
            , 'updatedAt' => $this->getUpdatedAt() ? $this->getUpdatedAt()->format('Y-m-d H:i:s') : ''
        ];
    }

    public function toExpose(): string
    {
        // TODO: Implement toExpose() method.
    }
}