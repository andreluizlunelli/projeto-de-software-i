<?php
/**
 * Created by PhpStorm.
 * User: André Luiz
 * Date: 30/08/2017
 * Time: 18:40
 */

namespace Src\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="mesa")
 *
 * Class Mesa
 * @package Src\Model\Entity
 */
class Mesa implements ToExpose
{
    use TimestampTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $numero;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $assentos;

    /**
     * @ORM\OneToMany(targetEntity="Sugestao", mappedBy="mesa")
     *
     * @var Sugestao
     */
    private $sugestoes;

    public function __construct(int $numero, int $assentos)
    {
        $this->setCreatedAt(new \DateTime('now'));
        $this->setNumero($numero);
        $this->setAssentos($assentos);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getNumero(): int
    {
        return $this->numero;
    }

    /**
     * @param int $numero
     */
    public function setNumero(int $numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return Sugestao
     */
    public function getSugestoes(): Sugestao
    {
        return $this->sugestoes;
    }

    /**
     * @param Sugestao $sugestoes
     */
    public function setSugestoes(Sugestao $sugestoes)
    {
        $this->sugestoes = $sugestoes;
    }

    /**
     * @return int
     */
    public function getAssentos(): int
    {
        return $this->assentos;
    }

    /**
     * @param int $assentos
     */
    public function setAssentos(int $assentos)
    {
        $this->assentos = $assentos;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId()
            , 'numero' => $this->getNumero()
            , 'assentos' => $this->getAssentos()
        ];
    }

    public function toExpose(): string
    {
        // TODO: Implement toExpose() method.
    }

    public function __toString(): string
    {
        return sprintf("Número: %02d; Assentos: %02d", $this->getNumero(), $this->getAssentos());
    }
}