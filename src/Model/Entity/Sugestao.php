<?php
/**
 * User: André Lunelli <andre@microton.com.br>
 * Date: 22/08/2017
 */

namespace Src\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Src\Model\Repository\SugestaoRepository")
 * @ORM\Table(name="sugestao")
 *
 * Class Sugestao
 * @package Src\Model\Entity
 */
class Sugestao implements ToExpose
{
    use TimestampTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $descricao;

    /**
     * @ORM\ManyToOne(targetEntity="Mesa", inversedBy="sugestoes")
     *
     * @var Mesa
     */
    private $mesa;

    /**
     * Sugestao constructor.
     * @param string $descricao
     * @param string $mesa
     */
    public function __construct(string $descricao, Mesa $mesa)
    {
        $this->setCreatedAt(new \DateTime('now'));
        $this->descricao = $descricao;
        $this->mesa = $mesa;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDescricao(): string
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao(string $descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return Mesa
     */
    public function getMesa(): Mesa
    {
        return $this->mesa;
    }

    /**
     * @param Mesa $mesa
     */
    public function setMesa(Mesa $mesa)
    {
        $this->mesa = $mesa;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId()
            , 'descricao' => $this->getDescricao()
            , 'mesa' => $this->getMesa()->getNumero()
            , 'createdAt' => $this->getCreatedAt()->format('Y-m-d H:i:s')
            , 'updatedAt' => $this->getUpdatedAt() ? $this->getUpdatedAt()->format('Y-m-d H:i:s') : ''
        ];
    }

    public function toExpose(): string
    {
        return json_encode($this->toArray());
    }
}