<?php
// ROTAS API
$app->group('/api', function () use ($app) {
    // TOKEN
    $app->post('/token', \Src\Controller\TokenController::class . ':post');

    // SUGESTÃO
    $app->get('/sugestao', \Src\Controller\SugestaoController::class . ':get');
    $app->get('/sugestao/{id}', \Src\Controller\SugestaoController::class . ':getOne');
    $app->post('/sugestao', \Src\Controller\SugestaoController::class . ':post');

    // PRATO
    $app->get('/prato', \Src\Controller\PratoController::class . ':get');
    $app->get('/prato/{id}', \Src\Controller\PratoController::class . ':getOne');
    $app->post('/prato', \Src\Controller\PratoController::class . ':post')
        ->add(new \Src\Middleware\AuthMiddleware());
    $app->put('/prato/{id}', \Src\Controller\PratoController::class . ':put')
        ->add(new \Src\Middleware\AuthMiddleware());
    $app->delete('/prato/{id}', \Src\Controller\PratoController::class . ':delete')
        ->add(new \Src\Middleware\AuthMiddleware());

    // MESA
    $app->get('/mesa', \Src\Controller\MesaController::class . ':get');
    $app->get('/mesa/{id}', \Src\Controller\MesaController::class . ':getOne');
    $app->post('/mesa', \Src\Controller\MesaController::class . ':post')
        ->add(new \Src\Middleware\AuthMiddleware());
    $app->put('/mesa/{id}', \Src\Controller\MesaController::class . ':put')
        ->add(new \Src\Middleware\AuthMiddleware());
    $app->delete('/mesa/{id}', \Src\Controller\MesaController::class . ':delete')
        ->add(new \Src\Middleware\AuthMiddleware());
});

// ADMIN
$app->group('/admin', function () use ($app) {
    $app->get('', \Src\Controller\Admin\SugestaoController::class . ':sugestoes');
    $app->get('/prato', \Src\Controller\Admin\PratoController::class . ':prato')->setName('admin-prato');
    $app->get('/mesa', \Src\Controller\Admin\MesaController::class . ':mesa')->setName('admin-mesa');
})->add(new \Src\Middleware\AutorizacaoMiddleware());

// TELAS SITE
$app->get('/', function ($request, $response, $args) {
    return $this->view->render($response, 'index.twig');
})->setName('home');

$app->get('/sugestao', \Src\Controller\Site\SugestaoController::class . ':apresentarForm')->setName('sugestao');
$app->get('/cardapio', \Src\Controller\Site\CardapioController::class . ':apresentarTabela')->setName('cardapio');
$app->any('/login', Src\Controller\Site\LoginController::class . ':login')->setName('login');
$app->any('/logoff', Src\Controller\Site\LoginController::class . ':logoff')->setName('logoff');
