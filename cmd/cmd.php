<?php

require __DIR__ . '/../vendor/autoload.php';

class cmd
{
    private $args = [];

    public function __construct(array $args)
    {
        $this->args = $args;
    }

    public function start()
    {
        $em = \Src\System\Database::getEntityManager();
        for ($i = 1; $i < 40; $i++) {
            $em->persist(new \Src\Model\Entity\Mesa($i, mt_rand(1, 22)));
        }
        $em->flush();
    }
}

(new cmd($argv))->start();


