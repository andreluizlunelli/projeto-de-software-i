<?php
/**
 * Created by PhpStorm.
 * User: André Luiz
 * Date: 21/08/2017
 * Time: 20:30
 */

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\Model\Entity\Mesa;
use Src\Model\Entity\Sugestao;
use Src\System\Database;

class PocTest extends TestCase
{
    public function testPocDoctrine()
    {
        $mesa = new Mesa();
        $mesa->setNumero(41);

        $sugestao = new Sugestao("pizza de abacaxi", $mesa);

        Database::getEm()->persist($mesa);
        Database::getEm()->persist($sugestao);
        Database::getEm()->flush($sugestao);

    }
}